import java.util.*;
import java.net.*;
import java.io.*;

import samplePackage.*;

class Solution{
	public static void main(String ar[]) throws Exception{
		try {
         Class cls = Class.forName("Solution");

         // returns the ClassLoader object
         ClassLoader cLoader = cls.getClassLoader();
       
         /* returns the Class object associated with the class or interface 
            with the given string name, using the given classloader. */
         Class cls2 = Class.forName("samplePackage.MainClass", true, cLoader);

         Object obj = cls2.newInstance();       
          
         // returns the name of the class
         System.out.println("Class = " + cls.getName());
         System.out.println("Class = " + cls2.getName()); 
      } catch(ClassNotFoundException ex) {
         System.out.println(ex.toString());
      }
	}
}