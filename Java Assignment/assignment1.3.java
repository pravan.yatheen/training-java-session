import java.util.*;
import java.io.*;

class Solution {

	static void displayReverseString(String method,StringBuffer inputString){
		System.out.println(method+": "+inputString.reverse());
	}

	public static void main(String ar[])throws Exception{
		StringBuffer inputString;

		if(ar.length!=0){
			inputString = new StringBuffer(ar[0]);
			displayReverseString("Command Line:",inputString);
		}
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a string: ");
		inputString = new StringBuffer(scan.next());
		displayReverseString("Scanner: ",inputString);

		System.out.println("Enter a string again: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		inputString = new StringBuffer(reader.readLine());
		displayReverseString("Buffered Reader: ",inputString);
	}
}