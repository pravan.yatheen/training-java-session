import java.lang.System;


class Solution{
	public static void main(String ar[]){
		System.out.println("Current Working Directory: "+System.getProperty("user.dir"));
		System.out.println("Operating System: "+System.getProperty("os.name"));
		System.out.println("Operating System Version: "+System.getProperty("os.version"));
	}
}